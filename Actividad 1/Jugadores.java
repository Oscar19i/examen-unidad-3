public class Jugadores{
private int uniforme;
private String nombre;
private String posicion;
private int goles;
private float minutos;

public Jugadores(){
	uniforme = 0;
	nombre = " ";
	posicion = " ";	
	goles = 0;
	minutos = 0.0f;
}

public String getNombre(){
	return nombre;
}

public void setNombre(String nombre){
	this.nombre = nombre;
}

public String getPosicion(){
	return posicion;
}	

public void setPosicion(String posicion){
	this.posicion = posicion;
}	
	
public int getUniforme(){
	return uniforme;
}
	
public void setUniforme(int uniforme){
	this.uniforme = uniforme;
}
	
public int getGoles(){
	return goles;
}

public void setGoles(int goles){
	this.goles = goles;
}
	
public float getMinutos(){
	return minutos;
}	

public void setMinutos(float minutos){
	this.minutos = minutos;
}
	
}