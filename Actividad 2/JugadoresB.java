public class JugadoresB{
private String nombre;
private int uniforme;
private float lanzamiento;
private float bateo;
private float defenza;

public JugadoresB(){
	nombre = " ";
	uniforme = 0;
	lanzamiento = 0.0f;
	bateo = 0.0f;
	defenza = 0.0f;	
}

public void setNombre(String nombre){
	this.nombre = nombre;
}

public String getNombre(){
	return nombre;
}

public void setUniforme(int uniforme){
	this.uniforme = uniforme;	
}

public int getUniforme(){
	return uniforme;
}

public void setLanzamiento(float lanzamiento){
	this.lanzamiento = lanzamiento;
}

public float getLanzamiento(){
	return lanzamiento;
}

public void setBateo(float bateo){
	this.bateo = bateo;
}

public float getBateo(){
	return bateo;
}

public void setDefenza(float Defenza){
	this.defenza = defenza;
}

public float getDefenza(){
	return defenza;
}
}